import { Environment } from '@rxap/nest-utilities';

export const environment: Environment = {
  name: 'swagger',
  app: 'status',
  production: true,
  swagger: true
};
